// const data = require('./data.json');

function splitIP(data) {
    const clonedData = JSON.parse(JSON.stringify(data));
    return clonedData.map((personObj) => {
        let outputArr = personObj.ip_address.split(".");
        let numOutputArr = outputArr.map((ipSection) => {
            const tempIPStore = ipSection;
            let strToNumIP = parseInt(ipSection);
            if (isNaN(strToNumIP)) {
                return tempIPStore;
            }
            else {
                return strToNumIP;
            }
        });
        personObj.ip_address = numOutputArr;
        return personObj;
    });
}

// console.log(splitIP(data));

module.exports = splitIP;