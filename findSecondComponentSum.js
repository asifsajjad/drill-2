// const data = require('./data.json');
const splitIP = require('./splitIP');

function findSecondComponentSum(data) {

    numifiedIPData = splitIP(data);
    return numifiedIPData.reduce((sum, personObj) => {
        return sum + personObj.ip_address[1];
    }, 0);
}

// console.log(findSecondComponentSum(data));

module.exports = findSecondComponentSum;
