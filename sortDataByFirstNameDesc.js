// const data = require('./data.json');

function sortDataByFirstNameDesc(data) {

    const clonedData = JSON.parse(JSON.stringify(data));

    return clonedData.sort((firstObj, SecondObj) => {
        const firstPersonName = firstObj.first_name.toLowerCase();
        const secondPersonName = SecondObj.first_name.toLowerCase();
        if (firstPersonName > secondPersonName) {
            return -1;
        }
        else if (firstPersonName < secondPersonName) {
            return 1;
        }

        return 0;
    });
}

// console.log(sortDataByFirstNameDesc(data));
module.exports = sortDataByFirstNameDesc;