// const data = require('./data.json');

function addFullName(data) {

    const clonedData = JSON.parse(JSON.stringify(data));

    return clonedData.map((personObj) => {
        let firstName = personObj.first_name;
        let lastName = personObj.last_name;
        personObj['full_name'] = firstName + ' ' + lastName;
        return personObj;
    });
}

// console.log(addFullName(data));
module.exports = addFullName;