// const data = require('./data.json');

function calculateEmailTypes(data) {

    return data.reduce((myObj, personObj) => {
        if (personObj.email.endsWith('.org')) {
            if (!myObj['.org']) {
                myObj['.org'] = 1;
            }
            else {
                myObj['.org'] += 1;
            }

        }
        if (personObj.email.endsWith('.au')) {
            if (!myObj['.au']) {
                myObj['.au'] = 1;
            }
            else {
                myObj['.au'] += 1;
            }
        }
        if (personObj.email.endsWith('.com')) {
            if (!myObj['.com']) {
                myObj['.com'] = 1;
            }
            else {
                myObj['.com'] += 1;
            }
        }
        return myObj;
    },{});

}

// console.log(calculateEmailTypes(data));
module.exports = calculateEmailTypes;