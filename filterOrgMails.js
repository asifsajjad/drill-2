// const data = require('./data.json');

function filterOrgMails(data) {

    const output = [];
    data.forEach((personObj) => {
        if (personObj.email.endsWith('.org')) {
            output.push(personObj.email);
        }
    });
    return output;
}

// console.log(filterOrgMails(data));

module.exports = filterOrgMails;