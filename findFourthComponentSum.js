// const data = require('./data.json');
const splitIP = require('./splitIP');

function findFourthComponentSum(data) {

    numifiedIPData = splitIP(data);
    return numifiedIPData.reduce((sum, personObj) => {
        return sum + personObj.ip_address[3];
    }, 0);
}

// console.log(findFourthComponentSum(data));

module.exports = findFourthComponentSum;